import Vue from 'vue'

import PureInput from './input'
Vue.component('PureInput', PureInput)

import PureButton from './button'
Vue.component('PureButton', PureButton)
