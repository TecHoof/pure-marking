# Changelog
All notable changes to this project will be documented in this file.

## 4.1.0 - 2018-10-11
### Add
- col-xs-hidden and col-hidden classes

## 4.0.0 - 2018-08-03
### Update
- Now library styles sizes based on em

## 3.0.0 - 2018-04-16
### Update
- Rewrite less -> sass
- Now library styles sizes based on vh
- Remove lists group styles
- Add grid example demo
- Now css build only in min version

## 2.0.3 - 2017-08-30
### Edit
- Removed extra margins from grid row